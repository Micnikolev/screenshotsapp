#ifdef __OBJC__
#import <Cocoa/Cocoa.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "FsprgEmbeddedStore.h"
#import "FsprgEmbeddedStoreController.h"
#import "FsprgEmbeddedStoreDelegate.h"
#import "FsprgOrderDocumentRepresentation.h"
#import "FsprgOrderView.h"
#import "FsprgStoreParameters.h"
#import "FsprgFileDownload.h"
#import "FsprgFulfillment.h"
#import "FsprgLicense.h"
#import "FsprgOrder.h"
#import "FsprgOrderItem.h"

FOUNDATION_EXPORT double FsprgEmbeddedStoreMacVersionNumber;
FOUNDATION_EXPORT const unsigned char FsprgEmbeddedStoreMacVersionString[];

