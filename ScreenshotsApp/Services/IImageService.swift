//
//  IImageService.swift
//  ScreenshotsApp
//
//  Created by Michael Nikolaev on 26.11.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import Cocoa

protocol IImageService {
    func getImage(completion: (_ image: NSImage?) -> Void)
}
