//
//  ImageService.swift
//  ScreenshotsApp
//
//  Created by Michael Nikolaev on 26.11.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import Cocoa
import Quartz

class ImageService: IImageService {
    func getImage(completion: (NSImage?) -> Void) {
        let panel = NSOpenPanel()
        panel.canChooseFiles = true
        panel.canChooseDirectories = false
        panel.allowsMultipleSelection = false
        
        if panel.runModal() == .OK {
            let url = panel.urls[0]
            guard let image = NSImage(contentsOf: url) else {
                return
            }
            
            completion(image)
        }
        
        completion(nil)
    }
    
    func chooseDirectory(completion: (URL?) -> Void) {
        let panel = NSOpenPanel()
        panel.canChooseFiles = false
        panel.canChooseDirectories = true
        panel.allowsMultipleSelection = false
        
        if panel.runModal() == .OK {
            let url = panel.urls[0]
            completion(url)
        }
        
        completion(nil)
    }
}
