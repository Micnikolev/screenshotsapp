//
//  AppDelegate.swift
//  ScreenshotsApp
//
//  Created by Michael Nikolaev on 14.11.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
    }

    func applicationWillTerminate(_ aNotification: Notification) {
    }


}

