//
//  RLMArchiveableObject.swift
//  ScreenshotsApp
//
//  Created by Michael Nikolaev on 10.01.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import RealmSwift

class RLMArchiveableObject : Object {
    @objc dynamic var data: Data?
    
    var object: Any? {
        get {
            return (data == nil) ? nil : NSKeyedUnarchiver.unarchiveObject(with: data!)
        }
        set {
            data = (newValue == nil) ? nil : NSKeyedArchiver.archivedData(withRootObject: newValue!)
        }
    }
    
    override class func ignoredProperties() -> [String] {
        return ["object"]
    }
}
