//
//  Screenshot.swift
//  ScreenshotsApp
//
//  Created by Michael Nikolaev on 28.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import Cocoa
import RealmSwift

class Screenshot: Object {
    @objc dynamic var screenshotImage: Data?
    @objc dynamic var backgroundImage: Data?
    @objc dynamic var layout: String!
    @objc dynamic var titleFontFamily: String! = "HelveticaNeue-Medium"
    @objc dynamic var subtitleFontFamily: String! = "HelveticaNeue-Medium"
    @objc dynamic var caption: String!
    @objc dynamic var fontSize: NSNumber! = 14.0
    @objc dynamic var subtitleSize: NSNumber! = 14.0
    @objc dynamic var subtitle: String!
    @objc dynamic var deviceColorName: String!
    @objc dynamic var textColor: Data!
    @objc dynamic var backgroundColor: Data!
    @objc dynamic var typeOfBezel: NSNumber! = 0
    @objc dynamic var gradient: NSData? = nil
}
