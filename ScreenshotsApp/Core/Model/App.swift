//
//  App.swift
//  ScreenshotsApp
//
//  Created by Michael Nikolaev on 28.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import Cocoa
import RealmSwift

enum platformType {
    case iOS, Android
}

class App: Object {
    var screenshots = List<Screenshot>()
    @objc dynamic var appName: String!
    @objc dynamic var version: String!
    @objc dynamic var time: Date = Date(timeIntervalSince1970: 1)
    @objc dynamic var platform: String!
}
