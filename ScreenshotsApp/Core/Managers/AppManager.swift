//
//  AppManager.swift
//  ScreenshotsApp
//
//  Created by Michael Nikolaev on 02.01.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import AppKit

extension CustomViewController {
    
    func openCustomizingVC(image: NSImage?, app: App?) {
        let storyboard = NSStoryboard(name: NSStoryboard.Name(rawValue: "AllScreenshotsVC"), bundle: nil)
        guard let nextViewController = storyboard.instantiateInitialController() as? AllScreenshotsVC else { return }
        nextViewController.deviceImage = image
        nextViewController.previousVC = type(of: self)
        nextViewController.app = app
        self.pushViewController(vc: nextViewController)
    }
    
    func openSetsVC() {
        let storyboard = NSStoryboard(name: NSStoryboard.Name(rawValue: "SetsViewController"), bundle: nil)
        guard let nextViewController = storyboard.instantiateInitialController() as? SetsViewController else { return }
        self.pushViewController(vc: nextViewController)
    }
    
    func openUpgradeToPro() {
        let vc = FastSpringController(nibName: NSNib.Name(rawValue: "FastSpringController"), bundle: nil)
        vc.title = "Upgrade to PRO"
        self.presentViewControllerAsModalWindow(vc)
    }
    
}
