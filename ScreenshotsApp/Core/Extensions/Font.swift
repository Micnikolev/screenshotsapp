//
//  Font.swift
//  ScreenshotsApp
//
//  Created by Michael Nikolaev on 23.01.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Cocoa

extension NSFont {
    enum screenshot {
        static func labelFont(size: Double) -> NSFont {
            return NSFont(name: "HelveticaNeue_Medium", size: CGFloat(size))!
        }
    }
}
