//
//  NSImage.swift
//  ScreenshotsApp
//
//  Created by Michael Nikolaev on 03.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import AppKit

extension NSImage {
    @discardableResult
    func saveAsPNG(url: URL, backgroundColor: NSColor) -> Bool {
        guard let tiffData = self.tiffRepresentation else {
            print("failed to get tiffRepresentation. url: \(url)")
            return false
        }
        let imageRep = NSBitmapImageRep(data: tiffData)
        guard let imageData = imageRep?.representation(using: .jpeg, properties: [.fallbackBackgroundColor:backgroundColor]) else {
            print("failed to get PNG representation. url: \(url)")
            return false
        }
        do {
            try imageData.write(to: url)
            return true
        } catch {
            print(error.localizedDescription)
            print("failed to write to disk. url: \(url)")
            return false
        }
    }
    
    public func getData() -> Data {
        let data = tiffRepresentation
        let rep = NSBitmapImageRep(data: data!)
        let imgData = rep!.representation(using: .jpeg, properties: [.compressionFactor : NSNumber(floatLiteral: 1.0)])
        return imgData!
    }
}
