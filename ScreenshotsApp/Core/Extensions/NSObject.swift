//
//  NSObject.swift
//  ScreenshotsApp
//
//  Created by Michael Nikolaev on 28.01.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation

extension NSObject {
    func convertToData() -> Data {
        return NSKeyedArchiver.archivedData(withRootObject: self)
    }
}
