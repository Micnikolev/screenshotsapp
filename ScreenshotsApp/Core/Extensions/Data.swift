//
//  Data.swift
//  ScreenshotsApp
//
//  Created by Michael Nikolaev on 10.01.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import AppKit

extension Data {
    func getObjectFromData<T>() -> T? {
        if let object = NSKeyedUnarchiver.unarchiveObject(with: self) as? T {
            return object
        }
        return nil
    }
}
