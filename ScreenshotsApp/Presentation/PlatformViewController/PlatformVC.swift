//
//  PlatformVC.swift
//  ScreenshotsApp
//
//  Created by Michael Nikolaev on 16.11.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import Cocoa

class PlatformVC: CustomViewController {

    @IBOutlet weak var iOSBackgroundView: NSView!
    @IBOutlet weak var androidBackgroundView: NSView!
    @IBOutlet weak var headerView: NSView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerView.wantsLayer = true
        headerView.layer?.backgroundColor = CGColor.white
        iOSBackgroundView.wantsLayer = true
        androidBackgroundView.wantsLayer = true
        iOSBackgroundView.layer?.backgroundColor = CGColor.white 
        androidBackgroundView.layer?.backgroundColor = CGColor.white
        iOSBackgroundView.layer?.cornerRadius = 4
        androidBackgroundView.layer?.cornerRadius = 4
    }
    
    func openNextController(platform: platformType) {
        let storyboard = NSStoryboard(name: NSStoryboard.Name(rawValue: "AddingScreenshotVC"), bundle: nil)
        guard let nextViewController = storyboard.instantiateInitialController() as? AddingScreenshotVC else { return }
        nextViewController.platform = platform
        self.pushViewController(vc: nextViewController)
    }
    
    @IBAction func iosPressed(_ sender: Any) {
        openNextController(platform: .iOS)
    }
    
    @IBAction func androidPressed(_ sender: Any) {
        openNextController(platform: .Android)
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.openSetsVC()
    }
    
    @IBAction func upgradeToProPressed(_ sender: Any) {
        self.openUpgradeToPro()
    }
}
