//
//  CustomzingVC.swift
//  ScreenshotsApp
//
//  Created by Michael Nikolaev on 26.11.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import Cocoa

class CustomizingVC: CustomViewController {

    @IBOutlet weak var imageLabel: NSTextField!
    @IBOutlet weak var deviceImageView: NSImageView!
    @IBOutlet weak var secondaryView: NSView!
    @IBOutlet weak var firstDeviceView: NSView!
    @IBOutlet weak var headerView: NSView!
    
    @IBOutlet var captionTextView: NSTextView!
    
    var deviceImage: NSImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerView.wantsLayer = true
        headerView.layer?.backgroundColor = CGColor.white
        
        deviceImageView.image = deviceImage
        
        self.secondaryView.wantsLayer = true
        self.secondaryView.layer?.backgroundColor = NSColor(deviceRed: 178/255, green: 215/255, blue: 251/255, alpha: 1.0).cgColor

        self.firstDeviceView.wantsLayer = true
        self.firstDeviceView.layer?.backgroundColor = NSColor.white.cgColor
        
        self.captionTextView.delegate = self
        
        save()
    }
    
    func save() {
        let dataOfView = secondaryView.dataWithPDF(inside: secondaryView.bounds)
        let imageOfView = NSImage(data: dataOfView)
        let paths = NSSearchPathForDirectoriesInDomains(.downloadsDirectory, .userDomainMask, true)
        guard let url = URL.init(string: paths[0]+"/testImage") else { return }
        imageOfView?.saveAsPNG(url: url, backgroundColor: NSColor(deviceRed: 178/255, green: 215/255, blue: 251/255, alpha: 1.0))
    }
    
    @IBAction func backPressed(_ sender: Any) {
        let storyboard = NSStoryboard(name: NSStoryboard.Name(rawValue: "AddingScreenshotVC"), bundle: nil)
        guard let nextViewController = storyboard.instantiateInitialController() as? AddingScreenshotVC else { return }
        self.pushViewController(vc: nextViewController)
    }
}

extension CustomizingVC: NSTextViewDelegate {
    func textDidChange(_ notification: Notification) {
        if let editor = notification.object as? NSTextView,
            editor == captionTextView {
            imageLabel.stringValue = captionTextView.string
        }
    }
}
