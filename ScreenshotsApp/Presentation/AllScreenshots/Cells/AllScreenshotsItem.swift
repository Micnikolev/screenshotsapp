//
//  AllScreenshotsItem.swift
//  ScreenshotsApp
//
//  Created by Michael Nikolaev on 03.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import Cocoa
import RxSwift
import RxCocoa

class AllScreenshotsItem: NSCollectionViewItem {
    
    @IBOutlet weak var imageLabel: NSTextField!
    @IBOutlet weak var subtitleLabel: NSTextField!
    @IBOutlet var subtitleTextView: NSTextView!
    @IBOutlet weak var deviceImageView: NSImageView!
    @IBOutlet weak var secondaryView: ACTGradientView!
    
    @IBOutlet weak var firstDeviceView: NSView!
    @IBOutlet var captionTextView: NSTextView!
    @IBOutlet weak var parametersStackView: NSStackView!
    @IBOutlet weak var deviceStackView: NSStackView!
    @IBOutlet weak var bezelImageView: NSImageView!
    
    @IBOutlet weak var deviceColorBox: SAComboBox!
    @IBOutlet weak var layoutBox: SAComboBox!
    
    @IBOutlet weak var textColorButton: SAColorButton!
    @IBOutlet weak var backgroundColorButton: SAColorButton!
    @IBOutlet weak var backgroundImageView: NSImageView!
    @IBOutlet weak var gradientView: ACTGradientEditor!
    @IBOutlet weak var backgroundDeviceView: NSView!
    @IBOutlet weak var parametersScrollView: NSScrollView!
    @IBOutlet weak var fontSlider: NSSlider!
    @IBOutlet weak var fontSizeLabel: NSTextField!
    @IBOutlet weak var subtitleSlider: NSSlider!
    @IBOutlet weak var suntitleFontSizeLabel: NSTextField!
    @IBOutlet weak var addBackgroundImageButtonView: SAImageButton!
    
    @IBOutlet weak var appNameViewPlaceholder: NSView!
    @IBOutlet weak var subtitleViewPlaceholder: NSView!
    @IBOutlet weak var captionFontFamilyComboBox: SAComboBox!
    @IBOutlet weak var subtitleFontFamilyComboBox: SAComboBox!
    
    var appNameView: SATextField!
    var subtitleView: SATextField!
    
    var imageService = ImageService()
    var saveView: ((_ index: Int, _ view: NSView)->Void)!
    
    var screenshotIndex: Int! {
        didSet {
            self.view.needsLayout = true
        }
    }
    
    var screenshot: Screenshot!
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        parametersScrollView.contentInsets = NSEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        if let textColor = imageLabel.textColor {
            textColorButton.colorWell.color = textColor
        }
        
        self.view.wantsLayer = true
        self.view.layer?.backgroundColor = NSColor(deviceRed: 245/255, green: 245/255, blue: 245/255, alpha: 1.0).cgColor
        
        self.secondaryView.wantsLayer = true
        self.secondaryView.layer?.backgroundColor = NSColor(deviceRed: 178/255, green: 215/255, blue: 251/255, alpha: 1.0).cgColor
        
        self.backgroundColorButton.colorWell.color = NSColor(cgColor:  (self.secondaryView.layer?.backgroundColor)!)!
        
        self.textColorButton.didChangedColor = {
            color in
            self.imageLabel.textColor = color
            self.subtitleLabel.textColor = color
        }
        
        self.backgroundColorButton.didChangedColor = {
            color in
            self.secondaryView.layer?.backgroundColor = color.cgColor
        }
        
        backgroundDeviceView.wantsLayer = true
//        backgroundDeviceView.layer?.backgroundColor = .white
        
        gradientView.delegate = self
        secondaryView.angle = 90
        
        let shadow = NSShadow()
        shadow.shadowColor = NSColor(deviceRed: 0, green: 0, blue: 0, alpha: 0.1)
        shadow.shadowOffset = CGSize(width: 5.0, height: -5.0)
        shadow.shadowBlurRadius = 0.2
        self.secondaryView.shadow = shadow
        
//        self.firstDeviceView.wantsLayer = true
//        self.firstDeviceView.layer?.backgroundColor = NSColor.white.cgColor
        
//        self.captionTextView.delegate = self
//        self.subtitleTextView.delegate = self
        
        deviceStackView.wantsLayer = true
        
        fontSlider.minValue = 12
        fontSlider.maxValue = 17
        
        subtitleSlider.minValue = fontSlider.minValue
        subtitleSlider.maxValue = fontSlider.maxValue
        
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: {[unowned self] _ in
            self.saveView(self.screenshotIndex, self.secondaryView)
        })
        
        addBackgroundImageButtonView.pressedOnButtonAction = {
            [unowned self] in
            self.imageService.getImage(completion: {
                [weak self] image in
                if let image = image {
                    self?.backgroundImageView.image = image
                }
            })
        }
        
        appNameView = SATextField(frame: NSRect(x: 0, y: 0, width: appNameViewPlaceholder.frame.width-60, height: 80))
        appNameView.isContinuous = true
        appNameView.target = self
        appNameView.action = #selector(appNameChanged)
        appNameViewPlaceholder.addSubview(appNameView)
        
        subtitleView = SATextField(frame: NSRect(x: 0, y: 0, width: appNameViewPlaceholder.frame.width-60, height: 80))
        subtitleView.isContinuous = true
        subtitleView.target = self
        subtitleView.action = #selector(subtitleChanged)
        subtitleViewPlaceholder.addSubview(subtitleView)
        
    }
    
    override func viewWillAppear() {
        super.viewWillAppear()
        
        setUpFamilyFonts()
        
        switch screenshot.typeOfBezel {
        case 0:
            bezelImageView.image = #imageLiteral(resourceName: "Apple iPhone 7 Matte Black")
        default:
            break
        }
        
        if let screenImg = screenshot.screenshotImage {
            deviceImageView.image = NSImage(data: screenImg)
        }
        imageLabel.stringValue = screenshot.caption
        
        
        self.secondaryView.wantsLayer = true
        
        imageLabel.textColor = screenshot.textColor.getObjectFromData()
        subtitleLabel.stringValue = screenshot.subtitle
        subtitleLabel.textColor = screenshot.textColor.getObjectFromData()

        appNameView.string = screenshot.caption
        subtitleView.string = screenshot.subtitle

        appNameView.endEditing()
        subtitleView.endEditing()
        
        layoutBox.stringValue = screenshot.layout
        textColorButton.colorWell.color = screenshot.textColor.getObjectFromData()!
        backgroundColorButton.colorWell.color = screenshot.backgroundColor.getObjectFromData()!
        deviceColorBox.stringValue = screenshot.deviceColorName
        fontSlider.doubleValue = Double(truncating: screenshot.fontSize)
        fontSizeLabel.stringValue = String(describing: Int(truncating: screenshot.fontSize))
        
        subtitleSlider.doubleValue = Double(truncating: screenshot.subtitleSize)
        suntitleFontSizeLabel.stringValue = String(describing: Int(truncating: screenshot.subtitleSize))
        
        
        if let imgData = screenshot.backgroundImage {
            backgroundImageView.image = NSImage(data: imgData)
        }

        if let gradient = screenshot.gradient {
            secondaryView.gradient = (gradient as Data).getObjectFromData()
            gradientView.gradient = (gradient as Data).getObjectFromData()
        } else {
            let backColor: NSColor = screenshot.backgroundColor.getObjectFromData()!
            self.secondaryView.layer?.backgroundColor = backColor.cgColor
        }
        
        imageLabel.font = NSFont.init(name: self.captionFontFamilyComboBox.stringValue, size: CGFloat(truncating: screenshot.fontSize))
        subtitleLabel.font = NSFont.init(name: self.subtitleFontFamilyComboBox.stringValue, size: CGFloat(truncating: screenshot.subtitleSize))
        
        fontSlider.rx.value.subscribe(onNext: {
            [weak self] value in
            DispatchQueue.main.async {
                if let font = self!.captionFontFamilyComboBox.objectValueOfSelectedItem as? String  {
                    self?.imageLabel.font = NSFont.init(name: font, size: CGFloat(value))
                }
                self?.fontSizeLabel.stringValue = String(describing: Int(self!.fontSlider.doubleValue))
                self?.imageLabel.sizeToFit()
                self?.subtitleLabel.sizeToFit()
            }
        }).disposed(by: disposeBag)
        
        subtitleSlider.rx.value.subscribe(onNext: {
            [weak self] value in
            DispatchQueue.main.async {
                if let font = self!.captionFontFamilyComboBox.objectValueOfSelectedItem as? String  {
                    self?.subtitleLabel.font = NSFont.init(name: font, size: CGFloat(value))
                }
                self?.suntitleFontSizeLabel.stringValue = String(describing: Int(self!.subtitleSlider.doubleValue))
                self?.imageLabel.sizeToFit()
                self?.subtitleLabel.sizeToFit()
            }
        }).disposed(by: disposeBag)
        
    }
    
    override func viewDidAppear() {
        super.viewDidAppear()
        
        parametersScrollView.contentInsets = NSEdgeInsets(top: 0, left: 0, bottom: 580, right: 0)
        
    }

    @IBAction func changeFontPressed(_ sender: Any) {
        let fontManager = NSFontManager.shared
        let fontPanel = fontManager.fontPanel(true)
        
        fontPanel?.makeKeyAndOrderFront(sender)
    }
    
    func setUpFamilyFonts() {
        captionFontFamilyComboBox.usesDataSource = false
        captionFontFamilyComboBox.removeAllItems()
        subtitleFontFamilyComboBox.removeAllItems()
        captionFontFamilyComboBox.addItems(withObjectValues: NSFontManager.shared.availableFonts)
        subtitleFontFamilyComboBox.addItems(withObjectValues: NSFontManager.shared.availableFonts)
        
        captionFontFamilyComboBox.stringValue = screenshot.titleFontFamily
        subtitleFontFamilyComboBox.stringValue = screenshot.subtitleFontFamily
        
        captionFontFamilyComboBox.delegate = self
        subtitleFontFamilyComboBox.delegate = self
    }

}

extension AllScreenshotsItem: NSTextViewDelegate {
    func textDidChange(_ notification: Notification) {
        if let editor = notification.object as? NSTextView{
            if editor == captionTextView {
                imageLabel.stringValue = captionTextView.string
            } else if editor == subtitleTextView {
                subtitleLabel.stringValue = subtitleTextView.string
            }
        }
    }
    
    @objc func appNameChanged() {
        imageLabel.stringValue = appNameView.string
    }
    
    @objc func subtitleChanged() {
        subtitleLabel.stringValue = subtitleView.string
    }
    

}

extension AllScreenshotsItem: ACTGradientEditorDelegate {
    func gradientEditorChangedGradient(_ editor: ACTGradientEditor!) {
        secondaryView.gradient = editor.gradient
    }
}

extension AllScreenshotsItem: NSComboBoxDelegate {
    func comboBoxSelectionDidChange(_ notification: Notification) {
        if let comboBox = notification.object as? SAComboBox {
            if let font = comboBox.objectValueOfSelectedItem as? String {
                if comboBox == captionFontFamilyComboBox {
                    imageLabel.font = NSFont(name: font, size: imageLabel.font!.pointSize)
                }
                else if comboBox == subtitleFontFamilyComboBox {
                    subtitleLabel.font = NSFont(name: font, size: subtitleLabel.font!.pointSize)
                }
            }
        }
    }
}
