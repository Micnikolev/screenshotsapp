//
//  NewScreenshotItem.swift
//  ScreenshotsApp
//
//  Created by Michael Nikolaev on 07.01.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Cocoa

class NewScreenshotItem: NSCollectionViewItem {

    @IBOutlet weak var dragView: SADragAndDropView!
    @IBOutlet weak var pointsButton: SAPointsButton!
    var pressedOnItem: (()->Void)!
    var draggedImage: ((_ image: NSImage)->Void)!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dragView.gotFile = {
            [weak self] path in
            let image = NSImage(byReferencingFile: path)
            if let image = image {
                self?.draggedImage(image)
            }
        }
        self.pointsButton.frameForPoints = self.pointsButton.frame
        self.dragView.wantsLayer = true
        self.dragView.layer?.backgroundColor = .white
        self.dragView.layer?.cornerRadius = 5
    }
    
    override func viewDidLayout() {
        super.viewDidLayout()
        self.pointsButton.frameForPoints = self.pointsButton.frame
    }
    
    @IBAction func shoosePressed(_ sender: Any) {
        pressedOnItem()
    }
}
