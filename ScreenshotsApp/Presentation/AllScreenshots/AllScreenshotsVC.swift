//
//  AllScreenshotsVC.swift
//  ScreenshotsApp
//
//  Created by Michael Nikolaev on 03.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import Cocoa
import RealmSwift
import FsprgEmbeddedStoreMac

class AllScreenshotsVC: CustomViewController, NSCollectionViewDelegate, NSCollectionViewDataSource {

    @IBOutlet weak var mainScrollVIew: NSScrollView!
    @IBOutlet weak var mainCollectionView: NSCollectionView!
    @IBOutlet weak var headerView: NSView!
    @IBOutlet weak var appNameTextField: NSTextField!
    @IBOutlet weak var versionTextField: NSTextField!
    @IBOutlet weak var appNameWidthConstraint: NSLayoutConstraint!
    
    var deviceImage: NSImage!
    
    let imageService = ImageService()
    
    var platform = "iOS"
    
    var screenshots = [Screenshot]()
    var previousVC: CustomViewController.Type!
    
    var screenshotViews = [NSView]()
    
    var app: App?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let app = app {
            appNameTextField.stringValue = app.appName
            versionTextField.stringValue = app.version
            for screenshot in app.screenshots {
                screenshots.append(screenshot)
                screenshotViews.append(NSView())
            }
        } else {
            let screenshot = Screenshot(value: ["bezelImage": #imageLiteral(resourceName: "Apple iPhone 7 Matte Black").getData(),
                                                "screenshotImage": deviceImage.getData(),
                                                "backgroundImage": nil,
                                                "layout": "Cropped Device with Text Above",
                                                "caption": "Replace this text with your own",
                                                "subtitle": "",
                                                "deviceColorName": "Black Device",
                                                "textColor": NSColor.white.convertToData(),
                                                "backgroundColor": NSColor(deviceRed: 178/255, green: 215/255, blue: 251/255, alpha: 1.0).convertToData()])
            screenshots.append(screenshot)
            screenshotViews.append(NSView())
        }
        
        configureCollectionView()
        
        appNameTextField.alignment = .center
        headerView.wantsLayer = true
        headerView.layer?.backgroundColor = CGColor.white
        
        mainCollectionView.register(AllScreenshotsItem.self, forItemWithIdentifier: NSUserInterfaceItemIdentifier(rawValue: "CollectionViewItem"))
        mainCollectionView.register(NewScreenshotItem.self, forItemWithIdentifier: NSUserInterfaceItemIdentifier(rawValue: "AddScreenshotItem"))
        
        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
        
        appNameTextField.refusesFirstResponder = true
        versionTextField.refusesFirstResponder = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(appNameChanged), name: NSControl.textDidChangeNotification, object: appNameTextField)
        
    }
    
    override func viewDidDisappear() {
        super.viewDidDisappear()
        
        NotificationCenter.default.removeObserver(self, name: NSControl.textDidChangeNotification, object: appNameTextField)
    }
    
    override func mouseDown(with event: NSEvent) {
        appNameTextField.abortEditing()
        versionTextField.abortEditing()
    }
    
    fileprivate func configureCollectionView() {
        let flowLayout = NSCollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
//        flowLayout.itemSize = NSSize(width: 280, height: 1500)
//        flowLayout.sectionInset = NSEdgeInsets(top: 0.0, left: 0.0, bottom: -580, right: 0.0)
        flowLayout.minimumInteritemSpacing = 20.0
        flowLayout.minimumLineSpacing = 20.0
        mainCollectionView.collectionViewLayout = flowLayout
        view.wantsLayer = true
        mainCollectionView.layer?.backgroundColor = NSColor.black.cgColor
    }
    
    func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
        return screenshots.count+1
    }
    
    func numberOfSections(in collectionView: NSCollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        var identifier = "CollectionViewItem"
        if indexPath.item == screenshots.count {
            identifier = "AddScreenshotItem"
        }
        
        let item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: identifier), for: indexPath)
        
        if let collectionViewItem = item as? AllScreenshotsItem {
            let screenshot = screenshots[indexPath.item]
            
            collectionViewItem.screenshot = screenshot
            
            collectionViewItem.screenshotIndex = indexPath.item
            
            collectionViewItem.saveView = {
                [weak self] index, view in
                self?.screenshotViews[index] = view
            }
            
            return collectionViewItem
        } else if let addNewItem = item as? NewScreenshotItem {
            addNewItem.pressedOnItem = {
                let imgService = ImageService()
                imgService.getImage(completion: {
                    [unowned self] image in
                    if let image = image {
                        let screenshot = Screenshot(value: ["bezelImage": #imageLiteral(resourceName: "Apple iPhone 7 Matte Black").getData(),
                                                            "screenshotImage": image.getData(),
                                                            "backgroundImage": nil,
                                                            "layout": "Cropped Device with Text Above",
                                                            "caption": "Replace this text with your own",
                                                            "subtitle": "",
                                                            "deviceColorName": "Black Device",
                                                            "textColor": NSColor.white.convertToData(),
                                                            "backgroundColor": NSColor(deviceRed: 178/255, green: 215/255, blue: 251/255, alpha: 1.0).convertToData()])
                        self.screenshots.append(screenshot)
                        self.screenshotViews.append(NSView())
                        
                        self.mainCollectionView.reloadData()
                    }
                })
            }
            
            addNewItem.draggedImage = {
                [unowned self] image in
                let screenshot = Screenshot(value: ["bezelImage": #imageLiteral(resourceName: "Apple iPhone 7 Matte Black").getData(),
                                                    "screenshotImage": image.getData(),
                                                    "backgroundImage": nil,
                                                    "layout": "Cropped Device with Text Above",
                                                    "caption": "Replace this text with your own",
                                                    "subtitle": "",
                                                    "deviceColorName": "Black Device",
                                                    "textColor": NSColor.white.convertToData(),
                                                    "backgroundColor": NSColor(deviceRed: 178/255, green: 215/255, blue: 251/255, alpha: 1.0).convertToData()])
                self.screenshots.append(screenshot)
                self.screenshotViews.append(NSView())
                
                self.mainCollectionView.reloadData()
            }
            
            return addNewItem
        }
        
        return item
    }
    
    func save(savingView: NSView, url: URL, index: Int) {
        let dataOfView = savingView.dataWithPDF(inside: savingView.bounds)
        let imageOfView = NSImage(data: dataOfView)
        imageOfView?.saveAsPNG(url: url.appendingPathComponent("Exported screenshot \(index).jpeg"), backgroundColor: NSColor(deviceRed: 178/255, green: 215/255, blue: 251/255, alpha: 1.0))
    }
    
    @IBAction func exportPressed(_ sender: Any) {
        
        imageService.chooseDirectory(completion: {
            url in
            guard let url = url else { return }
            for (i, view) in screenshotViews.enumerated() {
                save(savingView: view, url: url, index: i)
            }
        })

    }
    
    @IBAction func savePressed(_ sender: Any) {
        let finalScreenshots = List<Screenshot>()
        for index in screenshots.indices {
            let item = mainCollectionView.item(at: IndexPath(item: index, section: 0))
            guard let collectionViewItem = item as? AllScreenshotsItem else { return }
            
            let screenshot = Screenshot()
            screenshot.typeOfBezel = NSNumber.init(value: collectionViewItem.layoutBox.indexOfSelectedItem)
            screenshot.backgroundImage = collectionViewItem.backgroundImageView.image?.getData()
            screenshot.screenshotImage = collectionViewItem.deviceImageView.image?.getData() ?? nil
            screenshot.layout = collectionViewItem.layoutBox.stringValue
            screenshot.caption = collectionViewItem.appNameView.string
            screenshot.subtitle = collectionViewItem.subtitleView.string
            screenshot.deviceColorName = collectionViewItem.deviceColorBox.stringValue
            screenshot.textColor = collectionViewItem.textColorButton.colorWell.color.convertToData()
            screenshot.backgroundColor = collectionViewItem.backgroundColorButton.colorWell.color.convertToData()
            screenshot.fontSize = NSNumber.init(value: Float((collectionViewItem.imageLabel.font?.pointSize)!))
            screenshot.subtitleSize = NSNumber.init(value: Float((collectionViewItem.subtitleLabel.font?.pointSize)!))
            screenshot.titleFontFamily = collectionViewItem.captionFontFamilyComboBox.stringValue
            screenshot.subtitleFontFamily = collectionViewItem.subtitleFontFamilyComboBox.stringValue
            
            if let gradient = collectionViewItem.secondaryView.gradient {
                screenshot.gradient = gradient.convertToData() as NSData
            }
            
            finalScreenshots.append(screenshot)
        }
        
        let app = App()
        app.appName = appNameTextField.stringValue
        app.version = versionTextField.stringValue
        app.time = Date()
        app.platform = platform
        app.screenshots = finalScreenshots
        
        let realm = try! Realm()
        try! realm.write {
            if let app = self.app {
                realm.delete(app)
            }
            realm.add(app)
        }
        
        self.openSetsVC()
    }
    
    @IBAction func upgradeToProPressed(_ sender: Any) {
        self.openUpgradeToPro()
    }
    
    @IBAction func backPressed(_ sender: Any) {
        let storyboard = NSStoryboard(name: NSStoryboard.Name(rawValue: String(describing: previousVC.self!)), bundle: nil)
        guard let nextViewController = storyboard.instantiateInitialController() as? CustomViewController else { return }
        self.pushViewController(vc: nextViewController)
    }
    
    
}

extension AllScreenshotsVC {
    @objc func appNameChanged() {
        appNameWidthConstraint.constant =  appNameTextField.attributedStringValue.size().width + 8
        appNameTextField.setNeedsDisplay()
    }
}

extension AllScreenshotsVC: NSCollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: NSCollectionView, layout collectionViewLayout: NSCollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> NSSize {
        if indexPath.item == screenshots.count {
            return NSSize(width: 500, height: 1216)
        }
        
        return NSSize(width: 280, height: 1130)
    }
}
