//
//  FastSpringController.swift
//  ScreenshotsApp
//
//  Created by Michael Nikolaev on 26.01.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Cocoa
import WebKit
import AddressBook
import FsprgEmbeddedStoreMac

class FastSpringController: NSViewController {

    @IBOutlet weak var webView: WebView!
    
    let storeController = FsprgEmbeddedStoreController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        storeController.setDelegate(self)
        storeController.setWebView(webView)
        loadStore()
    }
    
    func loadStore() {
        let parameters = FsprgStoreParameters(raw: ["orderProcessType": kFsprgOrderProcessDetail, "storeId": "nbz-jak-019", "productId": "screenshot-app", "mode": kFsprgModeTest])
        storeController.load(with: parameters!)
    }
}

extension FastSpringController: FsprgEmbeddedStoreDelegate {
    func didLoadStore(_ url: URL!) {
        print(#function)
    }
    
    func didLoadPage(_ url: URL!, of pageType: FsprgPageType) {
        print(#function)
    }
    
    func didReceive(_ order: FsprgOrder!) {
        print(#function)
    }
    
    func view(withFrame frame: NSRect, for order: FsprgOrder!) -> NSView! {
        return NSView(frame: self.view.frame)
    }

    func webView(_ sender: WebView!, didFailLoadWithError error: Error!, for frame: WebFrame!) {
        print("didFailLoadWithError with error \(error.localizedDescription)")
    }
    
    func webView(_ sender: WebView!, didFailProvisionalLoadWithError error: Error!, for frame: WebFrame!) {
        print("didFailProvisionalLoadWithError with error \(error.localizedDescription)")
    }
}
