//
//  SATextField.swift
//  ScreenshotsApp
//
//  Created by Michael Nikolaev on 04.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Cocoa
import ZKTextField

class SATextField: ZKTextField {
    override init(frame: NSRect) {
        super.init(frame: frame)
        
        placeholderString = ""
        backgroundColor = NSColor.white
        shouldShowFocus = false
    }
    
    required init?(coder decoder: NSCoder) {
        super.init(coder: decoder)
    }
    
    override func drawFrame(with rect: NSRect) {
        // 2px border
        NSColor.gray.set()
        currentClippingPath().lineWidth = 1.0
        currentClippingPath().stroke()
    }
    
    override func textOffset(forHeight textHeight: CGFloat) -> NSPoint {
        // center vertically
        return NSMakePoint(12.0, round(NSMidY(bounds) - textHeight / 2))
    }
    
    override func textWidth() -> CGFloat {
        // the size of our field minus the margin on both size
        return bounds.size.width - 12 * 2
    }
    
    var insertionPointColor: NSColor {
        return NSColor(deviceRed: 0.5086, green: 0.5047, blue: 0.520, alpha: 1.000)
    }
    
    var minimumWidth: CGFloat {
        return 200
    }

    override func minimumHeight() -> CGFloat {
        return 39.0
    }

    override func maximumWidth() -> CGFloat {
        return 600
    }

    override func maximumHeight() -> CGFloat {
        return minimumHeight()
    }

}
