//
//  SAGhostButton.swift
//  ScreenshotsApp
//
//  Created by Michael Nikolaev on 06.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import Cocoa

class SAGhostButton: NSButton {

    var textColor = NSColor(red: 70/255, green: 146/255, blue: 232/255, alpha: 1.0)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.wantsLayer = true
        self.layer?.backgroundColor = CGColor(red: 70/255, green: 146/255, blue: 232/255, alpha: 1.0)
        
        if let font = font
        {
            self.layer?.cornerRadius = 4
            self.layer?.masksToBounds = true
            let style = NSMutableParagraphStyle()
            style.alignment = .center
            
            let attributes =
                [
                    NSAttributedStringKey.foregroundColor: textColor,
                    NSAttributedStringKey.font: font,
                    NSAttributedStringKey.paragraphStyle: style
            ]
            
            let attributedTitle = NSAttributedString(string: title, attributes: attributes)
            self.attributedTitle = attributedTitle
        }
    }
    
}
