//
//  SAColorButton.swift
//  ScreenshotsApp
//
//  Created by Michael Nikolaev on 13.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import Cocoa

class SAColorButton: NSView {

    @IBOutlet var contentView: NSView!
    @IBOutlet weak var internalView: NSView!
    @IBOutlet weak var colorWell: NSColorWell!
    @IBOutlet weak var colorField: NSTextField!
    
    var didChangedColor: ((_ color: NSColor) -> Void)?
    
    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        commonInit()
    }
    
    required init?(coder decoder: NSCoder) {
        super.init(coder: decoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed(NSNib.Name(rawValue: "SAColorButton"), owner: self, topLevelObjects: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.height, .width]
        
        internalView.wantsLayer = true
        internalView.layer?.backgroundColor = CGColor.white
        internalView.layer?.cornerRadius = 4
        
    }
    
    @IBAction func colorChanged(_ sender: NSColorWell) {
        if let didChangedColor = didChangedColor {
            didChangedColor(sender.color)
            self.colorField.stringValue = sender.color.getHexString()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
}
