//
//  SAButton.swift
//  ScreenshotsApp
//
//  Created by Michael Nikolaev on 05.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import Cocoa

@IBDesignable
class SAButton: NSButton {
    
    @IBInspectable var textColor: NSColor = NSColor.white
    @IBInspectable var fillColor: NSColor = NSColor(red: 70/255, green: 146/255, blue: 232/255, alpha: 1.0)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.wantsLayer = true
        self.layer?.backgroundColor = fillColor.cgColor
        
        if let font = font
        {
            self.layer?.cornerRadius = 4
            self.layer?.masksToBounds = true
            let style = NSMutableParagraphStyle()
            style.alignment = .center
            
            let attributes =
                [
                    NSAttributedStringKey.foregroundColor: textColor,
                    NSAttributedStringKey.font: font,
                    NSAttributedStringKey.paragraphStyle: style
                    ]
            
            let attributedTitle = NSAttributedString(string: title, attributes: attributes)
            self.attributedTitle = attributedTitle
        }
    }
    
    override func mouseDown(with event: NSEvent) {
        self.layer?.backgroundColor = NSColor(red: 47/255, green: 72/255, blue: 130/255, alpha: 1.0).cgColor
        super.mouseDown(with: event)
        self.layer?.backgroundColor = fillColor.cgColor
    }
    
}
