//
//  SAPointsButton.swift
//  ScreenshotsApp
//
//  Created by Michael Nikolaev on 06.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import Cocoa

class SAPointsButton: NSButton {

    var frameForPoints: CGRect? {
        didSet {
            setUpCreateSetBtn()
        }
    }
    
    var lineWidth: CGFloat = 8.0 {
        didSet {
            setUpCreateSetBtn()
        }
    }
    
    var frameDelta: CGFloat = 40.0 {
        didSet {
            setUpCreateSetBtn()
        }
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setUpCreateSetBtn()
    }
    
    func setUpCreateSetBtn() {
        let yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = NSColor(deviceRed: 0.0, green: 0.0, blue: 0.0, alpha: 0.09).cgColor
        yourViewBorder.lineWidth = lineWidth
        yourViewBorder.lineDashPattern = [10, 10]
        var borderFrame: CGRect!
        
        if let frameForPoints = frameForPoints {
            borderFrame = CGRect(x: frameDelta/2, y: frameDelta/2, width: frameForPoints.size.width-frameDelta, height: frameForPoints.size.height-frameDelta)
            
        } else {
            borderFrame = self.frame
        }
        yourViewBorder.frame = borderFrame
        yourViewBorder.fillColor = nil
        yourViewBorder.path = CGPath(rect: borderFrame, transform: nil)
        self.layer = yourViewBorder
    }
}
