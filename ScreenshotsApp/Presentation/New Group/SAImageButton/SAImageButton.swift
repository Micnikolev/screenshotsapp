//
//  SAImageButton.swift
//  ScreenshotsApp
//
//  Created by Michael Nikolaev on 31.01.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Cocoa

class SAImageButton: NSView {
    
    @IBOutlet weak var contentView: NSView!
    @IBOutlet weak var internalView: NSView!
    
    var pressedOnButtonAction: (() -> Void)?
    
    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        commonInit()
    }
    
    required init?(coder decoder: NSCoder) {
        super.init(coder: decoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed(NSNib.Name(rawValue: "SAImageButton"), owner: self, topLevelObjects: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.height, .width]
        
        internalView.wantsLayer = true
        internalView.layer?.backgroundColor = CGColor.white
        internalView.layer?.cornerRadius = 4
        
        internalView.layer?.borderColor = CGColor.black
        internalView.layer?.borderWidth = 0.2
    }
    @IBAction func pressedOnButton(_ sender: Any) {
        if let pressedOnButtonAction = pressedOnButtonAction {
            pressedOnButtonAction()
        }
    }
}
