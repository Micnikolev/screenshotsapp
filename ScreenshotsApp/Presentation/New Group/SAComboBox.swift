//
//  SAComboBox.swift
//  ScreenshotsApp
//
//  Created by Michael Nikolaev on 13.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import Cocoa

class SAComboBox: NSComboBox {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.autoresizingMask = .maxYMargin
        self.itemHeight = self.itemHeight + 3
    }
}
