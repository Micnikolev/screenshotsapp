//
//  AddingScreenshotVC.swift
//  ScreenshotsApp
//
//  Created by Michael Nikolaev on 26.11.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import Cocoa
import Quartz

class AddingScreenshotVC: CustomViewController {

    @IBOutlet weak var centerButton: SAPointsButton!
    @IBOutlet weak var uploadView: NSView!
    @IBOutlet weak var headerView: NSView!
    @IBOutlet weak var dragListenerView: SADragAndDropView!
    
    var platform: platformType!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        centerButton.frameForPoints = centerButton.frame
        centerButton.lineWidth = 3
        centerButton.frameDelta = 0
        
        headerView.wantsLayer = true
        headerView.layer?.backgroundColor = CGColor.white
        
        uploadView.wantsLayer = true
        uploadView.layer?.backgroundColor = CGColor.white
        uploadView.layer?.cornerRadius = 4
        
        dragListenerView.wantsLayer = true
        dragListenerView.layer?.cornerRadius = 4
        dragListenerView.gotFile = {
            [unowned self] path in
            let image = NSImage(byReferencingFile: path)
            if let image = image {
                self.openCustomizingVC(image: image, app: nil)
            }
        }
        dragListenerView.registerForDraggedTypes([.fileContents])
    }
    
    @IBAction func choosePressed(_ sender: Any) {
        let imgService = ImageService()
        imgService.getImage(completion: {
            [unowned self] image in
            if let image = image {
                self.openCustomizingVC(image: image, app: nil)
            }
        })
    }
    
    
    @IBAction func backPressed(_ sender: Any) {
        let storyboard = NSStoryboard(name: NSStoryboard.Name(rawValue: "PlatformVC"), bundle: nil)
        guard let nextViewController = storyboard.instantiateInitialController() as? PlatformVC else { return }
        self.pushViewController(vc: nextViewController)
    }
    
    @IBAction func upgradeToProPressed(_ sender: Any) {
        self.openUpgradeToPro()
    }
    
    override func viewDidLayout() {
        super.viewDidLayout()
        
        centerButton.frameForPoints = centerButton.frame
    }
    
    func getImage(completion: (_ image: NSImage) -> Void) {
        let vc = IKPictureTaker()
        vc.runModal()
        guard let image = vc.outputImage() else { return }
        completion(image)
    }
    
}
