//
//  SetsViewModel.swift
//  ScreenshotsApp
//
//  Created by Michael Nikolaev on 12.01.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import AppKit
import RealmSwift
import RxSwift
import RxCocoa

class SetsViewModel: NSObject {
    private weak var vc: SetsViewController!
    public var apps: Variable<[App]> = Variable([])
    
    init(vc: SetsViewController) {
        self.vc = vc
    }
    
    public func loadApps() {
        do {
            let realm = try Realm()
            
            let apps = realm.objects(App.self).sorted(byKeyPath: "time", ascending: false)
            for app in apps {
                self.apps.value.append(app)
            }
        } catch {
            print(error)
        }
    }
    
    private func deleteApp(at indexPath: IndexPath) {
        let app = apps.value[indexPath.item]
        showDeleteAlert(with: app.appName + " " + app.version) {
            [unowned self] answer in
            if answer {
                do {
                    let realm = try Realm()
                    try realm.write {
                        realm.delete(app)
                        self.apps.value.remove(at: indexPath.item)
                        self.vc.mainCollectionView.reloadData()
                        self.vc.setUpCenterScreen(appsCount: self.apps.value.count)
                    }
                } catch {
                    print(error)
                }
            }
        }
    }
    
    private func showDeleteAlert(with name: String, completion : (Bool)->Void) {
        let alert = NSAlert.init()
        alert.messageText = "Do you really want to delete the this screenshot set?"
        alert.addButton(withTitle: "Cancel")
        alert.addButton(withTitle: "Yes, Delete")
        completion(alert.runModal() == NSApplication.ModalResponse.alertFirstButtonReturn)
    }
}

extension SetsViewModel: NSCollectionViewDelegate, NSCollectionViewDataSource {
    
    func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
        return apps.value.count
    }
    
    func numberOfSections(in collectionView: NSCollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        let app = apps.value[indexPath.item]

        let item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "SetCollectionItem"), for: indexPath)
        guard let collectionViewItem = item as? SetCollectionItem else {return item}
        collectionViewItem.appNameLbl.stringValue = app.appName + " " + app.version
        collectionViewItem.dateLbl.stringValue = app.time.convertToString(format: "MM/dd/yyyy")
        collectionViewItem.indexPath = indexPath
        collectionViewItem.deleteButtonPressed = {
            [unowned self] indexPath in
            self.deleteApp(at: indexPath)
        }
        if let screenshot = app.screenshots[0].screenshotImage {
            collectionViewItem.screenshotImageView.image = NSImage(data: screenshot)
        }
        collectionViewItem.platformLbl.stringValue = app.platform
        
        return collectionViewItem
    }
    
    func collectionView(_ collectionView: NSCollectionView, didSelectItemsAt indexPaths: Set<IndexPath>) {
        let itemIndexPath = indexPaths[indexPaths.index(indexPaths.startIndex, offsetBy: 0)]
        let app = apps.value[itemIndexPath.item]
        vc.openCustomizingVC(image: nil, app: app)
    }
}
