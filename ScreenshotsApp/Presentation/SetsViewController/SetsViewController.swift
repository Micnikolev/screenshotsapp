//
//  ViewController.swift
//  ScreenshotsApp
//
//  Created by Michael Nikolaev on 14.11.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import Cocoa
import RealmSwift
import RxSwift

class SetsViewController: CustomViewController {

    @IBOutlet weak var createSetsBtn: SAPointsButton!
    @IBOutlet weak var mainCollectionView: NSCollectionView!
    @IBOutlet weak var mainCollectionScrollView: NSScrollView!
    
    let disposeBag = DisposeBag()
    
    private var setsViewModel: SetsViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setsViewModel = SetsViewModel(vc: self)
        
        setsViewModel.apps.asObservable().subscribe(onNext: {
            [weak self] apps in
            self?.mainCollectionView.reloadData()
            self?.setUpCenterScreen(appsCount: apps.count)
        }).disposed(by: disposeBag)
        
        Realm.Configuration.defaultConfiguration.deleteRealmIfMigrationNeeded = true
        
        mainCollectionView.delegate = setsViewModel
        mainCollectionView.dataSource = setsViewModel
        
        mainCollectionView.register(SetCollectionItem.self, forItemWithIdentifier: NSUserInterfaceItemIdentifier(rawValue: "SetCollectionItem"))
        
        self.configureCollectionView()
        
        setsViewModel.loadApps()
    }
    
    override func viewWillAppear() {
        super.viewWillAppear()
        
        var frame = self.view.window?.frame
        frame?.size = NSSize(width: (NSScreen.main?.frame.size.width)!-200, height: (NSScreen.main?.frame.size.height)!-200)
        self.view.window?.setFrame(frame!, display: true)
        self.view.window?.center()
        self.view.window?.minSize = (frame?.size)!
        
        self.createSetsBtn.frameForPoints = self.createSetsBtn.frame
        self.mainCollectionView.wantsLayer = true
        self.mainCollectionView.layer?.backgroundColor = NSColor(deviceRed: 245/255, green: 245/255, blue: 245/255, alpha: 1.0).cgColor
    }

    override var representedObject: Any? {
        didSet {
        }
    }
    
    override func viewDidLayout() {
        super.viewDidLayout()
        self.createSetsBtn.frameForPoints = self.createSetsBtn.frame
    }
    
    func setUpCenterScreen(appsCount: Int) {
        if appsCount == 0 {
            mainCollectionScrollView.isHidden = true
            createSetsBtn.isHidden = false
        } else {
            mainCollectionScrollView.isHidden = false
            createSetsBtn.isHidden = true
            self.mainCollectionView.reloadData()
        }
    }
    
    fileprivate func configureCollectionView() {
        let flowLayout = NSCollectionViewFlowLayout()
        flowLayout.itemSize = NSSize(width: 480, height: 202)
        flowLayout.sectionInset = NSEdgeInsets(top: 0.0, left: 165.0, bottom: 0.0, right: 165.0)
        flowLayout.minimumInteritemSpacing = 0.0
        flowLayout.minimumLineSpacing = 20.0
        mainCollectionView.collectionViewLayout = flowLayout
        view.wantsLayer = true
        mainCollectionView.layer?.backgroundColor = NSColor.black.cgColor
    }
    
    @IBAction func upgradeToProPressed(_ sender: Any) {
        self.openUpgradeToPro()
    }
    
    @IBAction func takePicturePressed(_ sender: NSButton) {
        let storyboard = NSStoryboard(name: NSStoryboard.Name(rawValue: "PlatformVC"), bundle: nil)
        guard let nextViewController = storyboard.instantiateInitialController() as? PlatformVC else { return }
        self.pushViewController(vc: nextViewController)
    }
}
