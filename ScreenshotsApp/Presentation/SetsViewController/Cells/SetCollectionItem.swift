//
//  SetCollectionItem.swift
//  ScreenshotsApp
//
//  Created by Michael Nikolaev on 28.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import Cocoa

class SetCollectionItem: NSCollectionViewItem {

    @IBOutlet weak var appNameLbl: NSTextField!
    @IBOutlet weak var dateLbl: NSTextField!
    @IBOutlet weak var platformLogoVImageView: NSImageView!
    @IBOutlet weak var screenshotImageView: NSImageView!
    @IBOutlet weak var platformLbl: NSTextField!
    @IBOutlet weak var shadowView: NSView!
    
    var indexPath: IndexPath!
    
    var deleteButtonPressed: ((_ indexPath: IndexPath) -> Void)!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dropShadow = NSShadow()
        dropShadow.shadowColor = NSColor(deviceRed: 0.0, green: 0.0, blue: 0.0, alpha: 0.3)
        dropShadow.shadowOffset = NSSize(width: 0, height: -20)
        dropShadow.shadowBlurRadius = 10
        
        view.wantsLayer = true
        view.shadow = dropShadow
        view.layer?.cornerRadius = 10
        view.layer?.backgroundColor = NSColor.white.cgColor
    }
    
    @IBAction func deletePressed(_ sender: Any) {
        deleteButtonPressed(indexPath)
    }
}
