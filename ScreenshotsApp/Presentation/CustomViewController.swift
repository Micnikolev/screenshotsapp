//
//  CustomViewController.swift
//  ScreenshotsApp
//
//  Created by Michael Nikolaev on 16.11.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import Cocoa

class CustomViewController: NSViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.layer?.backgroundColor = NSColor(deviceRed: 245/255, green: 245/255, blue: 245/255, alpha: 1.0).cgColor
    }
    
    func pushViewController(vc: NSViewController) {
        if let window = view.window {
            vc.view.frame = CGRect(x: 0, y: 0, width: Int(window.frame.width), height: Int(window.frame.height))
        }
        
        view.window?.contentViewController = vc
    }
}
